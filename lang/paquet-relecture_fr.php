<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Module: paquet-relecture
// Langue: fr

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// R
	'relecture_nom' => 'Relecture',
	'relecture_description' => '',
	'relecture_slogan' => 'Organiser la relecture de vos articles',
);
?>
